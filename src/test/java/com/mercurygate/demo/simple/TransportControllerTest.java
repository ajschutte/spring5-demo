package com.mercurygate.demo.simple;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.mercurygate.demo.DemoSpringWebApplicationInitializer;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DemoSpringWebApplicationInitializer.class})
@WebMvcTest(value = TransportController.class, secure = false)
public class TransportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransportService transportService;

    @Before
    public void setup() {

        Mockito
                .when(
                        transportService.findTransportById((Mockito.anyString()))).thenAnswer(new Answer<Transport>() {

            @Override
            public Transport answer(InvocationOnMock invocation) throws Throwable {
                Object[] arguments = invocation.getArguments();

                if (arguments != null && arguments.length > 0 && arguments[0] != null){

                    String transportId = (String) arguments[0];
                    return createTestTransport(transportId);
                }
                return null;
            }
        });

        Mockito
                .when(
                        transportService.findAllTransports()).thenReturn(createTestTransports()
                );

    }

    @Test
    public void retrieveAllCourses_expectedResponse() throws Exception {

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(
                "/transports").accept(
                        MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "[{\"transportId\":\"test\"}]";

        JSONAssert.assertEquals(expected, result.getResponse()
                                                .getContentAsString(), false);
    }

    @Test
    public void retrieveCourseById_expectedResponse() throws Exception {

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(
                        "/transports/9999").accept(
                        MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "{\"transportId\":\"9999\"}";

        JSONAssert.assertEquals(expected, result.getResponse()
                                                .getContentAsString(), false);
    }

    private List<Transport> createTestTransports() {
        return Arrays.asList(createTestTransport("test"));
    }

    private Transport createTestTransport(String id) {
        return new Transport()
                .withTransportId(id);
    }

}
