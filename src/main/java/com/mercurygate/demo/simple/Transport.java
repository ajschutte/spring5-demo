package com.mercurygate.demo.simple;

public class Transport {

    private String transportId;

    public String getTransportId() {
        return transportId;
    }

    public Transport withTransportId(String transportId) {
        this.transportId = transportId;
        return this;
    }

}
