package com.mercurygate.demo.simple;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StandardTransportService implements TransportService {

    public StandardTransportService() { }

    @Override
    public List<Transport> findAllTransports() {
        return Arrays.asList(createDummyTransport());
    }

    @Override
    public Transport findTransportById(String transportId) {
        return createDummyTransport()
                .withTransportId(transportId);
    }

    private Transport createDummyTransport() {
        return new Transport()
                .withTransportId("1234");
    }

}
