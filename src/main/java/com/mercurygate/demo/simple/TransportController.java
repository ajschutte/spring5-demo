package com.mercurygate.demo.simple;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Lazy
@RestController
public class TransportController {

    private static final Logger logger = LoggerFactory.getLogger(TransportController.class);

    @Value("${myapp.dburl}")
    private String dburl;

    @Autowired
    private TransportService transportService;

    public TransportController() {
        logger.info("Default No-Arg C'tor - Transport Service not resolved yet: {} nor property: {}", transportService, dburl);
    }

    @PostConstruct
    public void init() {
        logger.info("Postconstruct - Transport Service is already resolved: {} and property: {}", transportService, dburl);
    }

    @PreDestroy
    public void ishutdown() {
        logger.info("PreDestroy - Transport Service is still resolved: {}", transportService);
    }

    @GetMapping("/transports")
    public List<Transport> getAllTransports() {
        return transportService.findAllTransports();
    }

    @GetMapping("/transports/{transportId}")
    public Transport getTransportById(@PathVariable String transportId) {
        return transportService.findTransportById(transportId);
    }

}
