package com.mercurygate.demo.simple;

import java.util.List;

public interface TransportService {

    List<Transport> findAllTransports();

    Transport findTransportById(String transportId);

}
