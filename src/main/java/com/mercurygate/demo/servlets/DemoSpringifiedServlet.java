package com.mercurygate.demo.servlets;

import org.springframework.web.context.support.HttpRequestHandlerServlet;

import javax.servlet.annotation.WebServlet;

@WebServlet(description = "Http Servlet using pure java / annotations", urlPatterns = { "/demo-spring-servlet/*" },
        name = "demoServletHandler")
public class DemoSpringifiedServlet extends HttpRequestHandlerServlet { }
