package com.mercurygate.demo.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercurygate.demo.DemoServiceResponses;
import com.mercurygate.demo.scopes.DemoSingletonService;

@Component("demoServletHandler")
public class DemoHttpServletRequestHandler implements HttpRequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(DemoHttpServletRequestHandler.class);

    @Autowired
    private DemoSingletonService singleton;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        logger.info("INVOKING handleRequest(..) on servlet proxy..");

        DemoServiceResponses backend = singleton.demo(Optional.empty());

        resp.setContentType("application/json");

        String response =  getJsonRepresentationForResource(backend);

        resp.getWriter().append(response);

    }

    private String getJsonRepresentationForResource(Object resource) {

        try {
            return mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(resource);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

}
