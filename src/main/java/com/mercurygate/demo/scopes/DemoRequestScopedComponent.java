package com.mercurygate.demo.scopes;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DemoRequestScopedComponent {

    private static final Logger logger = LoggerFactory.getLogger(DemoRequestScopedComponent.class);

    private long initTimestamp;

    @PostConstruct
    public void init() {

        initTimestamp = System.currentTimeMillis();

        logger.info("REQUEST-SCOPED initialization: {}", initTimestamp) ;
    }

    @PreDestroy
    public void shutdown() {

        logger.info("REQUEST-SCOPED shutdown: {}", initTimestamp) ;

    }

    public long getInitTimestamp() {
        return initTimestamp;
    }

}
