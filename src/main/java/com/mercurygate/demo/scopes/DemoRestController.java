package com.mercurygate.demo.scopes;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import com.mercurygate.demo.DemoServiceResponses;

@RestController
@RequestMapping("/scopes")
public class DemoRestController {

    private static final Logger logger = LoggerFactory.getLogger(DemoRestController.class);

    @Autowired
    private DemoSingletonService singleton;

    @Autowired
    private HttpServletRequest httpRequest;

    @Autowired
    private DemoRequestScopedComponent requestScoped;

    @Autowired
    private DemoSessionScopedComponent sessionScoped;

    @GetMapping
    public DemoServiceResponses demo(@RequestParam Optional<Integer> serviceRequestCount) {

        logger.info("INVOKING doGet(..) on REST controller with HTTP request URI: {}..", httpRequest.getRequestURI());

        logger.info("REQUEST-SCOPED: {} and SESSION-SCOPED: {}", requestScoped.getInitTimestamp(), sessionScoped.getInitTimestamp());

        return singleton.demo(serviceRequestCount);

    }

}
