package com.mercurygate.demo.scopes;

import org.springframework.stereotype.Component;

import java.util.function.Supplier;

import com.mercurygate.demo.scopes.DemoPrototypeComponent;

@Component
public class DemoPrototypeComponentSupplier implements Supplier<DemoPrototypeComponent> {

    @Override
    public DemoPrototypeComponent get() {
        return new DemoPrototypeComponent();
    }

}
