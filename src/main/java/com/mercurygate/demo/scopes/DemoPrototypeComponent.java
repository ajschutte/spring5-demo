package com.mercurygate.demo.scopes;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.mercurygate.demo.DemoServiceResponse;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
//@Scope(value = BeanDefinition.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DemoPrototypeComponent {

    private static final Logger logger = LoggerFactory.getLogger(DemoPrototypeComponent.class);

    private static final AtomicInteger instanceCounter = new AtomicInteger(0);

    private int count;

    public DemoPrototypeComponent() {
        this.count = instanceCounter.incrementAndGet();
    }

    public DemoServiceResponse demo() {

        logger.info("INVOKING demo() on component instance: {}", this);

        DemoServiceResponse response = new DemoServiceResponse();
        response.setInstanceIdentifier(DemoPrototypeComponent.class.getSimpleName() + "_" + count);
        response.setTotalInstanceCount(instanceCounter.get());

        return response;

    }

}
