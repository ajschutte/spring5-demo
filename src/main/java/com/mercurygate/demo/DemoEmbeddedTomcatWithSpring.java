package com.mercurygate.demo;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class DemoEmbeddedTomcatWithSpring {

    private static final int PORT = getPort();

    public static void main(String[] args) throws Exception {

        String appBase = ".";
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(PORT);
        String baseDir = createTempDir();

        System.out.println("STARTING TOMCAT with base dir: " + baseDir);

        tomcat.setBaseDir(baseDir);
        tomcat.getHost().setAppBase(appBase);

        //Need to copy static HTML, JSPs, etc to tomcat dir..
        File source = FileUtils.getFile("./src/main/webapp/public");
        File dest = FileUtils.getFile(baseDir + "/public");
        if (!source.exists()) {
            throw new RuntimeException("static source web content not found..");
        }
        FileUtils.copyDirectory(source, dest);

        Context rootCtx = tomcat.addWebapp("", appBase);
        tomcat.start();

        System.out.println("STARTED TOMCAT with root context: " + rootCtx.getPath());

        tomcat.getServer().await();

    }

    private static String createTempDir() {
        try {
            File tempDir = File.createTempFile("tomcat.", "." + PORT);
            tempDir.delete();
            tempDir.mkdir();
            tempDir.deleteOnExit();
            return tempDir.getAbsolutePath();
        } catch (IOException ex) {
            throw new RuntimeException(
                    "Unable to create tempDir. java.io.tmpdir is set to " + System.getProperty("java.io.tmpdir"),
                    ex
            );
        }
    }

    private static int getPort() {
        String port = System.getenv("PORT");
        if (port != null) {
            return Integer.valueOf(port);
        }
        return 8080;
    }

}
