package com.mercurygate.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import com.fasterxml.jackson.databind.ObjectMapper;

// Designates this class as a Spring configuration class..
@Configuration
// Asks Spring to import the Spring MVC configurations..
@EnableWebMvc
// Asks Spring to scan the classpath for beans starting from the base package where this marker is located..
@ComponentScan(basePackageClasses = {DemoRoot.class})
public class DemoSpringWebApplicationInitializer implements WebApplicationInitializer {

    private static final Logger logger = LoggerFactory.getLogger(DemoSpringWebApplicationInitializer.class);

    /**
     * This gets called by {@link org.springframework.web.SpringServletContainerInitializer} at app bootstrap using the
     * standard Servlet 3.0 {@link javax.servlet.ServletContainerInitializer} mechanism..
     */
    @Override
    public void onStartup(ServletContext container) throws ServletException {

        logger.debug("Invoking onStartup(ServletContext container)..");

        // Creates a Spring annotation-driven application context..
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        // Registers this class as the initial Spring configuration with the application context..
        context.register(DemoSpringWebApplicationInitializer.class);

        // Creates and registers a Spring ContextLoaderListener - an implementation of a ServletContextListener,
        // passing the created application context. This will manage the lifecycle of the application context - starting and
        // stopping it when the servlet context is initialized or shutting down.
        // ServletContextListeners listen for emitted ServletContext events - in particular startup and shutdown events.
        container.addListener(new ContextLoaderListener(context));
        container.setInitParameter("contextInitializerClasses",
                                   YamlFileApplicationContextInitializer.class.getName());

        // Registers and maps the dispatcher servlet, and associates it with the created application context
        ServletRegistration.Dynamic dispatcher = container
                .addServlet("dispatcher", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        //All requests will get intercepted by Spring dispatcher servlet..
        dispatcher.addMapping("/*");

    }

    //Example bean creation via @Configuration - a Jackson ObjectMapper is just a non-Spring POJO but we want to inject it as a bean.
    @Bean
    ObjectMapper mapper() {
        return new ObjectMapper();
    }

    @Bean
    PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
