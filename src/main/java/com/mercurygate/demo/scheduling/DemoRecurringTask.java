package com.mercurygate.demo.scheduling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mercurygate.demo.simple.TransportService;

@Component
public class DemoRecurringTask {

    private static final Logger logger = LoggerFactory.getLogger(DemoRecurringTask.class);

    @Autowired
    private TransportService transportService;

    @Scheduled(fixedDelay = 1000, initialDelay = 1000)
    public void runTask() {

        logger.info("Running Task...with service: {}", transportService.findAllTransports());

    }

}
